use std::collections::HashMap;

use rocket_contrib::templates::tera::{try_get_value, Result};
use serde_json::value::{to_value, Value};

pub fn pretty_bytes(value: Value, _data: HashMap<String, Value>) -> Result<Value> {
    let mut bytes = try_get_value!("pretty_bytes", "value", f64, value);
    let byte_unit = ["B", "KB", "MB", "GB", "TB", "PB"];
    let mut result = "".to_string();

    for kind in byte_unit.iter() {
        let test_bytes = bytes / 1024.00_f64;
        if test_bytes < 1.00_f64 {
            result = format!("{:.2}", bytes) + kind;
            break;
        } else {
            bytes = test_bytes;
        }
    }

    Ok(to_value(result).unwrap())
}

pub fn dataset_to_url(value: Value, _data: HashMap<String, Value>) -> Result<Value> {
    let dataset = try_get_value!("dataset_to_url", "value", String, value);
    Ok(to_value(crate::utils::dataset_to_url(dataset)).unwrap())
}
