use crate::utils::convert_to_url;

use crate::rzfs::dataset::{DatasetDetail, DatasetList};

use rocket_contrib::templates::Template;

#[get("/all")]
pub fn all() -> Template {
    let datasets = DatasetList::new();
    Template::render("dataset/all", &datasets)
}

// TODO: Implment page that shows all datasets for a specific pool

#[get("/details/<dataset_name>")]
pub fn dataset_details(dataset_name: String) -> Template {
    let detail = DatasetDetail::new(DatasetList::new(), convert_to_url(dataset_name));

    Template::render("dataset/detail", &detail)
}
