pub fn convert_to_url(value: String) -> String {
    value.replace("/", "_")
}

pub fn dataset_to_url(value: String) -> String {
    value.replace("&#x2F;", "_")
}
