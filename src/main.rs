#![forbid(unsafe_code)]
//#![deny(missing_docs)]
#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate rocket;

mod dataset;
mod filters;
mod pool;
mod rzfs;
mod utils;

use rocket_contrib::helmet::SpaceHelmet;
use rocket_contrib::serve::StaticFiles;
use rocket_contrib::templates::tera::{FilterFn, Tera};
use rocket_contrib::templates::Template;
use rocket_prometheus::PrometheusMetrics;

trait RegisterExt {
    fn chainable_register_filter(&mut self, name: &str, filter: FilterFn) -> &mut Self;
}

impl RegisterExt for Tera {
    fn chainable_register_filter(&mut self, name: &str, filter: FilterFn) -> &mut Self {
        self.register_filter(name, filter);
        self
    }
}

fn main() {
    let prom = PrometheusMetrics::new();
    rocket::ignite()
        .attach(prom.clone())
        .attach(Template::custom(|engines| {
            engines
                .tera
                .chainable_register_filter("pretty_bytes", filters::pretty_bytes)
                .chainable_register_filter("dataset_to_url", filters::dataset_to_url);
        }))
        .attach(SpaceHelmet::default())
        .mount("/", routes![dataset::all])
        .mount("/metrics", prom)
        .mount("/dataset", routes![dataset::all])
        //.mount("/dataset", routes![dataset::pool_dataset_details])
        .mount("/dataset", routes![dataset::dataset_details])
        .mount("/pool", routes![pool::all])
        .mount("/pool", routes![pool::pool_details])
        .mount("/css", StaticFiles::from("css"))
        .mount("/scripts", StaticFiles::from("scripts"))
        .launch();
}
