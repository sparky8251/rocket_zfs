use std::process::Command;

use serde::Serialize;

fn convert_blanks(value: String) -> Option<String> {
    let mountpoint: Option<String>;
    if value == "-" {
        mountpoint = None
    } else {
        mountpoint = Some(value)
    };
    mountpoint
}

#[derive(Debug, Serialize)]
pub struct PoolList {
    pools: Vec<PoolListLine>,
}

#[derive(Debug, Serialize)]
pub struct PoolListLine {
    name: String,
    size: i64,
    allocated: i64,
    free: i64,
    checkpoint: Option<String>,
    expand_size: Option<String>,
    fragmentation: i8,
    capacity: i8,
    dedup_ratio: f32,
    health: String,
    alternate_root: Option<String>,
}

impl PoolList {
    pub fn new() -> Self {
        let output = Command::new("zpool")
            .args(&["list", "-H", "-p"])
            .output()
            .expect("failed to execute process");
        let stdout = String::from_utf8(output.stdout).unwrap();
        let mut list: Vec<&str> = stdout.split('\n').collect();
        list.pop(); // remove the final blank line

        let mut result: Vec<PoolListLine> = Vec::new();

        for line in list.iter() {
            let values: Vec<&str> = line.split('\t').collect();
            let line = PoolListLine::new(values);
            result.push(line)
        }
        PoolList { pools: result }
    }

    pub fn find_pool(self, pool_name: String) -> Option<PoolListLine> {
        let mut result: Option<PoolListLine> = None;
        for line in self.pools {
            if line.name == pool_name {
                result = Some(line);
                break;
            }
        }
        result
    }
}

impl PoolListLine {
    fn new(values: Vec<&str>) -> Self {
        PoolListLine {
            name: values[0].to_string(),
            size: values[1].parse::<i64>().unwrap(),
            allocated: values[2].parse::<i64>().unwrap(),
            free: values[3].parse::<i64>().unwrap(),
            checkpoint: convert_blanks(values[4].to_string()),
            expand_size: convert_blanks(values[5].to_string()),
            fragmentation: values[6].parse::<i8>().unwrap(),
            capacity: values[7].parse::<i8>().unwrap(),
            dedup_ratio: values[8].parse::<f32>().unwrap(),
            health: values[9].to_string(),
            alternate_root: convert_blanks(values[10].to_string()),
        }
    }
}
