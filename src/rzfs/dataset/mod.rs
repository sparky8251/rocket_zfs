use crate::utils::convert_to_url;

use std::process::Command;

use serde::Serialize;

fn convert_mountpoint(value: String) -> Option<String> {
    let mountpoint: Option<String>;
    if value == "none" {
        mountpoint = None
    } else {
        mountpoint = Some(value)
    };
    mountpoint
}

#[derive(Debug, Serialize)]
pub struct DatasetList {
    datasets: Vec<DatasetListLine>,
}

#[derive(Debug, Serialize)]
struct DatasetListLine {
    name: String,
    used: i64,
    available: i64,
    referenced: i64,
    mountpoint: Option<String>,
}

#[derive(Debug, Default, Serialize)]
pub struct DatasetDetail {
    name: String,
    used: i64,
    available: i64,
    total: i64,
    referenced: i64,
    mountpoint: Option<String>,
}

impl DatasetList {
    pub fn new() -> Self {
        let output = Command::new("zfs")
            .args(&["list", "-H", "-p"])
            .output()
            .expect("failed to execute process");
        let stdout = String::from_utf8(output.stdout).unwrap();
        let mut list: Vec<&str> = stdout.split('\n').collect();
        list.pop(); // remove the final blank line

        let mut result: Vec<DatasetListLine> = Vec::new();

        for line in list.iter() {
            let values: Vec<&str> = line.split('\t').collect();
            let line = DatasetListLine::new(values);
            result.push(line)
        }
        DatasetList { datasets: result }
    }
}

impl DatasetListLine {
    fn new(values: Vec<&str>) -> Self {
        DatasetListLine {
            name: values[0].to_string(),
            used: values[1].parse::<i64>().unwrap(),
            available: values[2].parse::<i64>().unwrap(),
            referenced: values[3].parse::<i64>().unwrap(),
            mountpoint: convert_mountpoint(values[4].to_string()),
        }
    }
}

impl DatasetDetail {
    pub fn new(values: DatasetList, dataset: String) -> Self {
        let mut result = DatasetDetail::default();

        for line in values.datasets.iter() {
            if convert_to_url(line.name.clone()) == dataset {
                result = Self {
                    name: line.name.clone(),
                    used: line.used,
                    available: line.available,
                    // TODO: This method is inaccurate on anything but the top dataset.
                    // Need to determine how to make this accurate even in the presence
                    // of several pools
                    total: line.used + line.available,
                    referenced: line.referenced,
                    mountpoint: line.mountpoint.clone(),
                }
            }
        }
        result
    }
}
