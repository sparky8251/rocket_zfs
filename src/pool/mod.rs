use crate::rzfs::pool::PoolList;

use rocket_contrib::templates::Template;

#[get("/all")]
pub fn all() -> Template {
    let pools = PoolList::new();
    Template::render("pool/all", &pools)
}

#[get("/<pool_name>")]
pub fn pool_details(pool_name: String) -> Template {
    let pools = PoolList::new();
    let pool = pools.find_pool(pool_name);
    match pool {
        Some(v) => Template::render("pool/detail", &v),
        None => unimplemented!() // TODO Implement appropriate 5xx handler
    }
}
